/**** DEBUT EXO 1 ***/
//1ère lettre en MAJ
console.log('           ');
console.log('EXO ----------- 1');
function ucfirst(str) {
    if (typeof str !== "string" || str.length === 0) return "";
    return str[0].toUpperCase() + str.substring(1).toLowerCase();
}

console.log(ucfirst('test'));
console.log(ucfirst('Test'));
console.log(ucfirst('3est'));
console.log(ucfirst('rest  rezf'));
console.log(ucfirst(''));
console.log(ucfirst(null));
console.log(ucfirst({}));

console.log('------------');
/**** FIN EXO 1 ***/


/**** DEBUT EXO 2 ***/
//1er lettre de  chaque mots en majuscule
console.log('           ');
console.log('EXO ----------- 2');
console.log(capitalize("capitalize cdsdQSDze capiFDFe"))
function capitalize(str) {

    if (typeof str !== "string" || str.length === 0) return "";

    //METHODE 1
    const array = str.toLowerCase().split(" ");
    for(let i=0; i< array.length;) {
        array[i] = ucfirst(array[i++]);
    }

    return array.join(" ");

    //METHODE 2
    return str
        .toLowerCase()
        .split(' ')
        .map(function(item) {
            return ucfirst(item);
        })
        .join(" ");
}

console.log('------------');
/**** FIN EXO 2 ***/



/**** DEBUT EXO 3 ***/
//Capitalize + coller les mots , "hello world" ==> "HelloWorld"
console.log('           ');
console.log('EXO ----------- 3');
var test3 = "hello world 68632 damn --8ERREUR89";
console.log(camelCase(test3));
function camelCase(test3) {
    if (typeof test3 !== "string" || test3.length === 0) return "";
    return test3.replace(/[^a-zA-Z]/gm, ' ').toLowerCase().split(' ').map(function (item) {
        return capitalize(item);
    }).join('');
}
console.log('------------');
/**** FIN EXO 3 ***/


/**** DEBUT EXO 4 (snake_case) ***/
//Joindre les mots par des underscores en MIN
console.log('           ');
console.log('EXO ----------- 4');
var myString = "toggle case is the coolest" ; // ==> voici_mon_texte
console.log(snake_case(myString));

function snake_case(myString) {
    if (typeof myString !== "string" || myString.length === 0) return "";
    return myString.toLowerCase().replace(/[^a-zA-Z0-9]/gi, '_');
}
console.log('------------');
/**** FIN EXO 4 (snake_case) ***/


/**** DEBUT EXO 5 (leet) ***/
//Cryptage (uniquement les voyelles) //A=>4, E=>3, I=>1, O=>0 ,U=> (_), Y=>7
console.log('           ');
console.log('EXO ----------- 5');
var string = "anaconda";
console.log(leet(string));
function leet(string) {
    if (typeof string !== "string" || string.length === 0) return "";
    return string.replace(/[aeiouy]/ig, function(car){
        switch (car) {
            case 'A':
            case 'a':
                return 4;
            case 'E':
            case 'e':
                return 3;
            case 'I':
            case 'i':
                return 1;
            case 'O':
            case 'o':
                return 0;
            case 'U':
            case 'u':
                return '(_)';
            case 'Y':
            case 'Y':
                return 4;
            default:
                return '';
        }
    });
}
console.log('------------');


/**** DEBUT EXO 6 ***/
console.log('           ');
console.log('EXO ----------- 6');

let test4 = {
    name: 'Samba',
    type: {
        name: 'lion',
        properties: {
            colors: {
                eyes: 'blue',
                hair: 'yellow'
            },
            height:{
                max: 150,
                min: 1
            }
        }
    }
};
console.log(prop_access(test4, "type.properties.height.max"));

function prop_access(obj, attribut){
    if(obj === null) { obj = {}; }
    if (typeof attribut !== "string" || attribut.length === 0 || attribut === null  ) return obj;
    let attributStop = [];
    let valueObjAttribut = attribut.split(".").reduce( function (prev, next) {
        if(prev) attributStop.push(next);
        return prev && prev[next];
    }, obj);
    if (valueObjAttribut === undefined){
        return console.log(attributStop+' not exist');
    }
    return valueObjAttribut;

}
console.log('------------');
/**** FIN EXO 6 ***/



/**** DEBUT EXO 7 ***/
console.log('           ');
console.log('EXO ----------- 7');

var test8 = "mots verlant";
console.log(verlan(test8));
function verlan(str){
    if (typeof str !== "string" || str.length === 0) return "";
    return str.split(" ").map(function (word){
        return word.split("").reverse().join('');
    }).join(" ");
}

console.log('------------');
/**** FIN EXO 7 ***/


/**** DEBUT EXO 8 ***/
console.log('           ');
console.log('EXO ----------- 8');

var test8 = "The word";
console.log(verlan(test8));
function yoda(str){
    if (typeof str !== "string" || str.length === 0) return "";
    return str.split(" ").reverse().join(" ");
}
console.log('------------');
/**** FIN EXO 8 ***/

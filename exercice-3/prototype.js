String.prototype.ucfirst = function() {
    if (this.length === 0) return "";
    return this[0].toUpperCase() + this.substring(1);
};

//console.log("Damnnn".ucfirst());



//-----------------------------------//

let test4 = {
    name: 'Samba',
    type: {
        name: 'lion',
        properties: {
            colors: {
                eyes: 'blue',
                hair: 'yellow'
            },
            height:{
                max: 150,
                min: 1
            }
        }
    }
};

Object.prototype.prop_access = function(attribut){
    const that = this;
    if(that === null) { that = {}; }
    if (typeof attribut !== "string" || attribut.length === 0 || attribut === null  ) return that;
    var valueObjAttribut = attribut.split(".").reduce( function (prev, next) {
        return prev && prev[next];
    }, that);
    if (valueObjAttribut === undefined){
        return console.log(attribut+' not exist');
    }
    return valueObjAttribut;

};

console.log(test4.prop_access('type.properties.height.max'));
